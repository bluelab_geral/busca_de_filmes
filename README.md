# busca_de_filmes

```
_            _   _         _     
| |_    ___  | | | |  ___  | |
| ' \  / -_) | | | | / _ \ |_|
|_||_| \___| |_| |_| \___/ (_)
                           
```

# Teste para candidatos à vaga da bolsa do Bluelab

### Background

Um novo cliente contratou o Bluelab para desenvolver uma aplicação WEB que informa detalhes sobre filmes, coisas como título, data de criação, diretor, entre alguns outros. O cliente deseja que a aplicação seja o mais simples possível, algo como digitar o nome do filme, em inglês, e que algumas informações sejam mostradas.

### Requisitos da página web

1. A sua página deve ser minimalista.

2. Os nomes dos filmes devem ser pesquisados em inglês.

3. Quando for feito a pesquisa do filme, deve retornar as seguintes informações: Title, Year, Runtime, Genre, Website. Caso o filme não tenha alguma dessas informações, deve aparecer a frase "Infelizmente não temos essa informação :(".


### Instruções

1. O projeto deve estar dividido em Front-end e Back-end.

2. É **obrigatório** o uso da API: OMDB API http://www.omdbapi.com para pesquisar sobre os filmes.

3. O projeto deve estar em algum repositório remoto com acesso publico. GitHub, BitBucket ou GitLab são alguns exemplos, mas utilizem o que lhes é confortável. O projeto deve ser entregue por meio do link do repositório, até a data definida.

4. As linguagens/frameworks/bibliotecas são de escolha do candidato, utilizem o que lhes é mais confortável para desenvolver.

5. Na parte do Front-end avaliamos tanto código como estética. Então, tanto suas páginas, como o seu código, devem estar de uma forma organizada.

6. Na parte do Back-end avaliamos a organização do código e se seu código é fácil de ser lido.

7. Lembre-se que vocês não vão estar presentes no momento da nossa análise de código, então todas as instruções para utilizar/rodar versões de framework e/ou biblioteca do seu projeto devem estar especificadas no README.md do seu repositório.

### Datas importantes e avisos

Os links dos repositórios devem ser enviados para o email *mafra@unifor.br* com o assunto *resolução do desafio* até o dia 01/02/2019 às 10:00, depois disso não será mais aceito o recebimento.

Caso apareça alguma dúvida ou empaque em alguma parte do caminho, podem procurar o apoio do pessoal do Bluelab pessoalmente ou no email *mafra@unifor.br*. Nós estamos aqui para ajudar!! Os candidatos podem trocar idéias de como resolver o problema, mas o que não será aceito é cópia de código. Então, lembrem-se: que isso é um desafio e que você está concorrendo com outras pessoas!!
O que mais esperamos é que vocês TENTEM realmente fazer o desafio!!

### Inspiração

1. https://weatherv2-220201.appspot.com